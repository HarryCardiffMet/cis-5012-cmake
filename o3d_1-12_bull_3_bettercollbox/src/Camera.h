#ifndef CAMERA_H
#define CAMERA_H

#include <OgreCamera.h>
#include <OgreSceneNode.h>

class Camera
{
public:
	Camera();
	virtual ~Camera();

	void update(float dt);

	Ogre::SceneNode* getNode() { return node; }
	Ogre::Camera* getOgreCamera() { return cam; }

private:
	void initViewports();

	float spd, turn_spd, pitch_spd;

	Ogre::Camera* cam;
	Ogre::SceneNode* node;
	Ogre::SceneNode* cam_node;
	Ogre::SceneNode* pitch_node;

};
#endif