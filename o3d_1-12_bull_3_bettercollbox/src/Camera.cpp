Camera::Camera()
    : spd(10),
    turn_spd(12),
    pitch_spd(4),
    node(0),
    cam_node(0),
    pitch_node(0)
{
    cam = GameManager::instance().scn_mgr->createCamera("UserCamera");
    cam->setNearClipDistance(.1);

    node = GameManager::instance().scn_mgr->getRootSceneNode()->createChildSceneNode();

    cam_node = node->createChildSceneNode();
    cam_node->setPosition(0, 1.8, 3);
    pitch_node = cam_node->createChildSceneNode();
    pitch_node->attachObject(cam);

    initViewports();

}

void Camera::initViewports()
{
    Ogre::Viewport* vp =
        GameManager::instance().window->addViewport(cam);
    vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

    cam->setAspectRatio(
        Ogre::Real(vp->getActualWidth()) /
        Ogre::Real(vp->getActualHeight()));
}

Camera::update
Ogre::Vector3 movement(0, 0, 0);
Ogre::Vector3 direction = node->getOrientation() * Ogre::Vector3::NEGATIVE_UNIT_Z;
direction.normalise();

GameManager::instance().player->runAnimation(false);

const Input input = GameManager::instance().input_system->input;

if (input.up)
movement += direction;
if (input.down)
movement -= direction;

if (input.right)
{
    movement.x -= direction.z;
    movement.z += direction.x;
}
if (input.left)
{
    movement.x += direction.z;
    movement.z -= direction.x;
}

if (movement.x == 0 && movement.z == 0)
{
    GameManager::instance().player->setAnimation("idle");
}
else
{
    movement.normalise();;
    node->translate(dt * spd * movement);
    GameManager::instance().player->setAnimation("walk");
}

node->yaw(Ogre::Degree(dt * -input.rot_x * turn_spd));
pitch_node->pitch(Ogre::Degree(dt * -input.rot_y * pitch_spd));

GameManager::instance().player->runAnimation(true);
