//------------------------------- Seek -----------------------------------
//
//  Given a target, this behavior returns a steering force which will
//  direct the agent towards the target
//------------------------------------------------------------------------
//void Seek(Ogre::Vector3 Player, Ogre::Quaternion TargetOrientation)
//{
//    Ogre::Vector3 mAgentPosition = mGlobalResource->LocalPlayerObjectScene->getInventoryPosition();
//    Ogre::Quaternion mAgentOrientation = mGlobalResource->LocalPlayerObjectScene->getInventoryOrientation();
//    Ogre::Vector3 mVectorToTarget = TargetPosition - mAgentPosition; // A-B = B->A
//    mAgentPosition.normalise();
//    mAgentOrientation.normalise();
//
//    Ogre::Vector3 mAgentHeading = mAgentOrientation * mAgentPosition;
//    Ogre::Vector3 mTargetHeading = TargetOrientation * TargetPosition;
//    mAgentHeading.normalise();
//    mTargetHeading.normalise();
//
//    // Orientation control - Ogre::Vector3::UNIT_Y is common up vector.
//    Ogre::Vector3 mAgentVO = mAgentOrientation.Inverse() * Ogre::Vector3::UNIT_Y;
//    Ogre::Vector3 mTargetVO = TargetOrientation * Ogre::Vector3::UNIT_Y;
//
//    // Compute new torque scalar (-1.0 to 1.0) based on heading vector to target.
//    Ogre::Vector3 mSteeringForce = mAgentOrientation.Inverse() * mVectorToTarget;
//    mSteeringForce;.normalise();
//
//    float mYaw = mSteeringForce.x;
//    float mPitch = mSteeringForce.y;
//    float mRoll = mTargetVO.getRotationTo(mAgentVO).getRoll().valueRadians();
//
//    clsSystemControls::getSingleton(){.setPitchControl(mPitch);
//    clsSystemControls::getSingleton().setYawControl(mYaw);
//    clsSystemControls::getSingleton().setRollControl(mRoll);
//
//} // Seek